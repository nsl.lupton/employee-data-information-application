package appEmployeeInfo.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import appEmployeeInfo.DAO.EmployeeDAO;
import appEmployeeInfo.core.*;

public class EmployeeSearchGui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private EmployeeDAO employeeDAO;
	private JTable table;

	// Launch the application.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					EmployeeSearchGui frame = new EmployeeSearchGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	// Create the frame.

	public EmployeeSearchGui() {
		setTitle("Employee Search Application");

		try {
			employeeDAO = new EmployeeDAO();
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);

		JLabel lblEnterLastName = new JLabel("Enter Last Name");
		panel.add(lblEnterLastName);

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);

		// Search Button
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {

					String lastName = textField.getText();
					List<Employee> employees = null;
					if (lastName != null && lastName.trim().length() > 0) {
						employees = employeeDAO.searchEmployees(lastName);
					} else {
						employees = employeeDAO.getAllEmployees();
					}
					EmployeeTableModel model = new EmployeeTableModel(employees);

					table.setModel(model);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(EmployeeSearchGui.this, "Error: " + exc, "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel.add(btnSearch);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		scrollPane.setViewportView(table);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);

		// Add Employee
		JButton btnAddEmployee = new JButton("Add Employee");
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// create dialog
				AddEmployee dialog = new AddEmployee(EmployeeSearchGui.this, employeeDAO);

				// show dialog
				dialog.setVisible(true);
			}
		});
		panel_1.add(btnAddEmployee);

		JButton btnUpdateEmployee = new JButton("Update Employee");
		btnUpdateEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get the selected item
				int row = table.getSelectedRow();
				
				// make sure a row is selected
				if (row < 0) {
					JOptionPane.showMessageDialog(EmployeeSearchGui.this, "You must select an employee", "Error",
							JOptionPane.ERROR_MESSAGE);				
					return;
			}
				// get the current employee
				Employee tempEmployee = (Employee) table.getValueAt(row, EmployeeTableModel.OBJECT_COL);
				
				// create dialog
				AddEmployee dialog = new AddEmployee(EmployeeSearchGui.this, employeeDAO, 
															tempEmployee, true);

				// show dialog
				dialog.setVisible(true);
			}
		});
		panel_1.add(btnUpdateEmployee);

		JButton btnDeleteInfo = new JButton("Delete Info");
		btnDeleteInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					// get the selected row
					int row = table.getSelectedRow();

					// make sure a row is selected
					if (row < 0) {
						JOptionPane.showMessageDialog(EmployeeSearchGui.this, 
								"You must select an employee", "Error", JOptionPane.ERROR_MESSAGE);				
						return;
					}
					// prompt the user
					int response = JOptionPane.showConfirmDialog(
							EmployeeSearchGui.this, "Delete this employee?", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response != JOptionPane.YES_OPTION) {
						return;
					}
					// get the current employee
					Employee tempEmployee = (Employee) table.getValueAt(row, EmployeeTableModel.OBJECT_COL);

					// delete the employee
					employeeDAO.deleteEmployee(tempEmployee.getId());

					// refresh GUI
					refreshEmployeesView();

					// show success message
					JOptionPane.showMessageDialog(EmployeeSearchGui.this,
							"Employee deleted succesfully.", "Employee Deleted",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(EmployeeSearchGui.this,
							"Error deleting employee: " + exc.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_1.add(btnDeleteInfo);
	}

	// Refresh
	public void refreshEmployeesView() {
		try {
			List<Employee> employees = employeeDAO.getAllEmployees();

			// create the model and update the "table"
			EmployeeTableModel model = new EmployeeTableModel(employees);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

}
