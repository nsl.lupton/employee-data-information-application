package appEmployeeInfo.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import appEmployeeInfo.core.*;
import appEmployeeInfo.DAO.*;

public class AddEmployee extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField firstNameTextField;
	private JTextField lastNameTextField;
	private JTextField emailTextField;
	private JTextField departmentTextField;
	private JTextField salaryTextField;
	
	private EmployeeDAO employeeDAO;

	private EmployeeSearchGui employeeSearchGui;
	
	private Employee previousEmployee = null;
	private boolean updateMode = false;
	
	public AddEmployee(EmployeeSearchGui theEmployeeSearchGui, EmployeeDAO theEmployeeDAO,Employee thePreviousEmployee, boolean theUpdateMode) {
		this();
		employeeDAO = theEmployeeDAO;
		employeeSearchGui = theEmployeeSearchGui;
		previousEmployee = thePreviousEmployee;
		
		updateMode = theUpdateMode;

		if (updateMode) {
			setTitle("Update Employee");
			
			populateGui(previousEmployee);
		}
	}
	private void populateGui(Employee theEmployee) {

		firstNameTextField.setText(theEmployee.getFirstName());
		lastNameTextField.setText(theEmployee.getLastName());
		emailTextField.setText(theEmployee.getEmail());
		departmentTextField.setText(theEmployee.getDepartment());
		salaryTextField.setText(theEmployee.getSalary().toString());		
	}

	public AddEmployee(EmployeeSearchGui theEmployeeSearchGui,
			EmployeeDAO theEmployeeDAO) {
		this(theEmployeeSearchGui, theEmployeeDAO, null, false);
	}

	 //Launch the application.

	public static void main(String[] args) {
		try {
			AddEmployee dialog = new AddEmployee();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void saveEmployee() {
		// get the employee info from gui
		String firstName = firstNameTextField.getText();
		String lastName = lastNameTextField.getText();
		String email = emailTextField.getText();
		String department =departmentTextField.getText();
		String salary = salaryTextField.getText();
		double salaryStr = Double.parseDouble(salary);
		
		Employee tempEmployee = null;
		
		if(updateMode) {
			tempEmployee = previousEmployee;
			tempEmployee.setLastName(lastName);
			tempEmployee.setFirstName(firstName);
			tempEmployee.setEmail(email);
			tempEmployee.setDepartment(department);
			tempEmployee.setSalary(salaryStr);
		}else {
			tempEmployee = new Employee(lastName,
				firstName, email, department, salaryStr);
		}
		try {
			// save to the database
			if(updateMode) {
			employeeDAO.addEmployee(tempEmployee);
			}else {
				employeeDAO.addEmployee(tempEmployee);
			}
			// close dialog
			setVisible(false);
			dispose();

			// refresh gui list
			employeeSearchGui.refreshEmployeesView();
			
			// show success message
			JOptionPane.showMessageDialog(employeeSearchGui,
					"Employee added succesfully.",
					"Employee Added",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(employeeSearchGui,"Error saving employee: "
							+ exc.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	 // Create the dialog.
	public AddEmployee() {
		setTitle("Add Employee Information");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(12, 12, 82, 15);
		contentPanel.add(lblFirstName);
		
		firstNameTextField = new JTextField();
		firstNameTextField.setBounds(97, 10, 329, 19);
		contentPanel.add(firstNameTextField);
		firstNameTextField.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(12, 43, 82, 15);
		contentPanel.add(lblLastName);
		
		lastNameTextField = new JTextField();
		lastNameTextField.setBounds(97, 41, 329, 19);
		contentPanel.add(lastNameTextField);
		lastNameTextField.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(12, 74, 66, 15);
		contentPanel.add(lblEmail);
		
		emailTextField = new JTextField();
		emailTextField.setBounds(97, 72, 329, 19);
		contentPanel.add(emailTextField);
		emailTextField.setColumns(10);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setBounds(12, 101, 98, 15);
		contentPanel.add(lblDepartment);
		
		departmentTextField = new JTextField();
		departmentTextField.setBounds(97, 99, 329, 19);
		contentPanel.add(departmentTextField);
		departmentTextField.setColumns(10);
		
		JLabel lblSalary = new JLabel("Salary");
		lblSalary.setBounds(12, 128, 66, 15);
		contentPanel.add(lblSalary);
		
		salaryTextField = new JTextField();
		salaryTextField.setBounds(97, 128, 329, 19);
		contentPanel.add(salaryTextField);
		salaryTextField.setColumns(10);
		
		JLabel lblAddEmployeeInto = new JLabel("ADD EMPLOYEE INTO DATABASE");
		lblAddEmployeeInto.setBounds(184, 216, 242, 15);
		contentPanel.add(lblAddEmployeeInto);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						saveEmployee();
					}
				});
				okButton.setActionCommand("Save");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
	
		}
	}
}
